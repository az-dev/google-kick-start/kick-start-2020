<?php
declare(strict_types = 1);
$DEBUG = false;

if ($DEBUG) {
  error_reporting(E_ALL);
}

function latest_day(int $n, int $d, array $buses) {
  global $DEBUG;

  $runs = array();

  if ($DEBUG) {
    echo sprintf("\tmax day = %s\n", $d);
    echo sprintf("\tdays = %s\n\n", implode(", ", $buses));
  }

  for ($i = 0; $i < count($buses); $i++) {
    $runs[$i] = array();

    for ($j = 1; $j <= $d; $j++) {
      if ($j % $buses[$i] === 0) {
        $runs[$i][$j] = 1;
      } else {
        $runs[$i][$j] = 0;
      }
    }
  }

  if ($DEBUG) {
    echo sprintf("\tRun matrix\n");
    for ($i = 0; $i < count($runs); $i++) {
      echo sprintf("\tX%s = %s\n", $i + 1, implode(", ", $runs[$i]));
    }
  }

  $max_day = $d;
  for ($i = count($runs) - 1; $i >= 0; $i--) {
    for ($j = $max_day; $j > 0; $j--) {
      if ($runs[$i][$j] === 1) {
        $max_day = $j;
        break;
      }
    }
  }

  return $max_day;
}

/**
* Instead of making a bus runs matrix and iterate over it, calculate directly
* on the same loop the data needed.
*
* @param int $n unused
* @param int $d The desired day the person wants to arrive
* @param array<int> The frequency run of each bus.
*
* @returns int The latest day the person can depart in order to arrive on $d day.
*/
function latest_day_improved(int $n, int $d, array $buses) {
  global $DEBUG;

  if ($DEBUG) {
    echo sprintf("\tmax day = %s\n", $d);
    echo sprintf("\tdays = %s\n\n", implode(", ", $buses));
  }

  $max_day = $d;
  for ($i = count($buses) - 1; $i >= 0; $i--) {
    for ($j = $max_day; $j > 0; $j--) {
      if ($j % $buses[$i] === 0) {
        $max_day = $j;
        break;
      }
    }
  }

  return $max_day;
}

function main() {
  $test_cases = (int) fgets(STDIN);
  $answers = array();

  for ($i = 0; $i < $test_cases; $i++) {
    list($n, $d) = array_map('intval', explode(" ", fgets(STDIN)));

    $days = array_map('intval', explode(" ", fgets(STDIN)));

    $answers[] = latest_day_improved($n, $d, $days);

    echo sprintf("Case #%s: %s\n", $i+1, $answers[$i]);
  }
}

main();
