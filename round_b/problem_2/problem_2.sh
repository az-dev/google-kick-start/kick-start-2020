#!/usr/bin/env bash

# Copyright (c) 2021 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
# License MIT
#
# Problem 2: Bus Routes
#
# https://codingcompetitions.withgoogle.com/kickstart/round/000000000019ffc8/00000000002d83bf

source problem_2.in

echo "$dataset1" | php problem_2.php >> problem_2.out
echo "================================================================================" >> problem_2.out
