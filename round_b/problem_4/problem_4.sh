#!/usr/bin/env bash

# Copyright (c) 2021 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
# License MIT
#
# Problem 4: Wandering Robot
#
# https://codingcompetitions.withgoogle.com/kickstart/round/000000000019ffc8/00000000002d8565

source problem_4.in

echo "$dataset1" | php problem_4.php >> problem_4.out
echo "================================================================================" >> problem_4.out
