<?php
declare(strict_types = 1);
$DEBUG = false;

if ($DEBUG) {
  error_reporting(E_ALL);
}

function evaluate(int $w, int $h, int $l, int $u, int $r, int $d, bool $debug = false) {

  return 0.12345;
}

function main(bool $debug = false): int {
  $test_cases = (int) fgets(STDIN);
  $answers = array();

  for ($i = 0; $i < $test_cases; $i++) {
    list($w, $h, $l, $u, $r, $d) = array_map('intval', explode(" ", fgets(STDIN)));

    $answers[] = evaluate($w, $h, $l, $u, $r, $d, $debug);

    echo sprintf("Case #%s: %s\n", $i+1, $answers[$i]);
  }

  return 0;
}

main($DEBUG);
