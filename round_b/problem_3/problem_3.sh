#!/usr/bin/env bash

# Copyright (c) 2021 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
# License MIT
#
# Problem 3: Robot Path Decoding
#
# https://codingcompetitions.withgoogle.com/kickstart/round/000000000019ffc8/00000000002d83dc

source problem_3.in

echo "$dataset1" | php problem_3.php >> problem_3.out
echo "================================================================================" >> problem_3.out

echo "$dataset2" | php problem_3.php >> problem_3.out
echo "================================================================================" >> problem_3.out
