<?php
declare(strict_types = 1);
$DEBUG = false;

if ($DEBUG) {
  error_reporting(E_ALL);
}

function expand(string $program, bool $debug = false): string {

  return $program;
}

/**
* Executes de given program. Internally it expands the program if necessary.
*
* @param string $program The program to execute.
* @param bool $debug Whether $DEBUG is true or not.
*
* @return array<int, int> The final position.
*/
function execute(string $program, bool $debug = false): array {
  $position = array('w' => 1, 'h' => 1);

  if (false) {
    $program = expand($program, $debug);
  }

  for ($i = 0; $i < strlen($program); $i++) {
    if ($program[$i] === 'N') {
      $position['h'] = $position['h'] - 1 > 0 ? $position['h'] - 1 : 1_000_000_000;
    } elseif ($program[$i] === 'S') {
      $position['h'] = $position['h'] + 1 > 1_000_000_000 ? 1 : $position['h'] + 1;
    } elseif ($program[$i] === 'E') {
      $position['w'] = $position['w'] + 1 > 1_000_000_000 ? 1 : $position['w'] + 1;
    } elseif ($program[$i] === 'W') {
      $position['w'] = $position['w'] - 1 > 0 ? $position['w'] - 1 : 1_000_000_000;
    }
  }

  return $position;
}

function main(bool $debug = false): int {
  $test_cases = (int) fgets(STDIN);
  $answers = array();

  for ($i = 0; $i < $test_cases; $i++) {
    $program = fgets(STDIN);

    $answers[] = execute($program, $debug);

    echo sprintf("Case #%s: %s %s\n", $i+1, $answers[$i]['w'], $answers[$i]['h']);
  }

  return 0;
}

main($DEBUG);
