<?php
declare(strict_types = 1);
$DEBUG = false;

if ($DEBUG) {
  error_reporting(E_ALL);
}

function peaks(array $heights) {
  global $DEBUG;

  $peaks = 0;

  if ($DEBUG) {
    echo sprintf("\theights = %s\n\n", implode(", ", $heights));
  }

  for ($i = 0; $i < count($heights) ; $i++) {
    if ($i === 0 || $i === count($heights) -1) {
      continue;
    } else {
      if ($heights[$i - 1] < $heights[$i] && $heights[$i + 1] < $heights[$i]) {
        $peaks++;
      }
    }
  }

  return $peaks;
}

function main() {
  $test_cases = (int) fgets(STDIN);
  $answers = array();

  for ($i = 0; $i < $test_cases; $i++) {
    $n = (int) fgets(STDIN);

    $heigths = array_map('intval', explode(" ", fgets(STDIN)));

    $answers[] = peaks($heigths);

    echo sprintf("Case #%s: %s\n", $i+1, $answers[$i]);
  }
}

main();
