#!/usr/bin/env bash

# Copyright (c) 2021 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
# License MIT
#
# Problem 1: Bike Tour
#
# https://codingcompetitions.withgoogle.com/kickstart/round/000000000019ffc8/00000000002d82e6

source problem_1.in

echo "$dataset1" | php problem_1.php >> problem_1.out
echo "================================================================================" >> problem_1.out
