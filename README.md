# Google Kick Start 2020

<hr />

[Google Kick Start 2020](https://codingcompetitions.withgoogle.com/kickstart/archive/2020)


[Google Kick Start 2020](https://codingcompetitions.withgoogle.com/kickstart/archive/2020) is solved using [PHP](https://www.php.net/).

<hr />

|  Round  |                                  Problem                                   |                                          Comments                                          |
|---------|----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------|
| Round A | :heavy_check_mark: [Problem 1: Allocation](round_a/problem_1/problem_1.md) | Algorithm is extremely easy. Sort array and then iterate through it while still on budget. |
| Round A | [Problem 2: Plates](round_a/problem_2/problem_2.md)                        |                                                                                            |
| Round A | [Problem 3: Workout](round_a/problem_3/problem_3.md)                       |                                                                                            |
| Round B | :heavy_check_mark: [Problem 1: Bike Tour](round_b/problem_1/problem_1.md)  | Algorithm is extremely easy. Compare before and after elements with current element.       |
| Round B | :heavy_check_mark: [Problem 2: Bus Routes](round_b/problem_2/problem_2.md) | Moderately difficult. Naive algorithm runs out of memory on large datasets. Improved algorithm runs out of time. It is believed that both implementations can finish on an unrestricted environment.                |
|         |                                                                            |                                                                                            |

## License

MIT License

Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
