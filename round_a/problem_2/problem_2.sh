#!/usr/bin/env bash

# Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
# License MIT
#
# Problem 2: plates
#
# https://codingcompetitions.withgoogle.com/kickstart/round/000000000019ffc7/00000000001d40bb

source problem_2.in

echo "$dataset1" | php problem_2.php >> problem_2.out
echo "================================================================================" >> problem_2.out
