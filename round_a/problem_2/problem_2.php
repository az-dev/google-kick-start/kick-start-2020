<?php
declare(strict_types = 1);
$DEBUG = true;

if ($DEBUG) {
  error_reporting(E_ALL);
}

function plates(int $n, int $k, int $p, array $stacks) {
  global $DEBUG;

  $max_value = 0;

  if ($DEBUG) {
    echo sprintf("\tn=%s k=%s p=%s\n", $n, $k, $p);
    foreach ($stacks as $stack) {
      echo sprintf("\t%s\n", implode(", ", $stack));
    }
  }

  //

  return $max_value;
}

function main() {
  $test_cases = (int) fgets(STDIN);
  $answers = array();

  for ($i = 0; $i < $test_cases; $i++) {
    list($n, $k, $p) = array_map('intval', explode(" ", fgets(STDIN)));

    $stacks = array();
    for ($j = 0; $j < $n; $j++) {
      $stacks[$j] = array_map('intval', explode(" ", fgets(STDIN)));
    }

    $answers[] = plates($n, $k, $p, $stacks);

    echo sprintf("Case #%s: %s\n", $i+1, $answers[$i]);
  }
}

main();
