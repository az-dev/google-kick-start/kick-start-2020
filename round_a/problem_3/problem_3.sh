#!/usr/bin/env bash

# Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
# License MIT
#
# Problem 3: Workout
#
# https://codingcompetitions.withgoogle.com/kickstart/round/000000000019ffc7/00000000001d3f5b

source problem_3.in

echo "$dataset1" | php problem_3.php >> problem_3.out
echo "================================================================================" >> problem_3.out

echo "$dataset2" | php problem_3.php >> problem_3.out
echo "================================================================================" >> problem_3.out
