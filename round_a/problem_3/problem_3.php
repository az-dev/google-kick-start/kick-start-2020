<?php
declare(strict_types = 1);
$DEBUG = true;

if ($DEBUG) {
  error_reporting(E_ALL);
}

function workout(int $n, int $k, array $program) {
  global $DEBUG;

  $difficulty = 0;

  if ($DEBUG) {
    echo sprintf("\tn=%s k=%s\n", $n, $k);
    echo sprintf("\t%s\n", implode(", ", $program));
  }

  //

  return $difficulty;
}

function main() {
  $test_cases = (int) fgets(STDIN);
  $answers = array();

  for ($i = 0; $i < $test_cases; $i++) {
    list($n, $k) = array_map('intval', explode(" ", fgets(STDIN)));
    $program = array_map('intval', explode(" ", fgets(STDIN)));

    $answers[] = workout($n, $k, $program);

    echo sprintf("Case #%s: %s\n", $i+1, $answers[$i]);
  }
}

main();
