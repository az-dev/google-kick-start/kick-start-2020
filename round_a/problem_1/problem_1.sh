#!/usr/bin/env bash

# Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
# License MIT
#
# Problem 1: Allocation
#
# https://codingcompetitions.withgoogle.com/kickstart/round/000000000019ffc7/00000000001d3f56

source problem_1.in

echo "$dataset1" | php problem_1.php >> problem_1.out
echo "================================================================================" >> problem_1.out
