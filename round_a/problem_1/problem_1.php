<?php
declare(strict_types = 1);
$DEBUG = false;

if ($DEBUG) {
  error_reporting(E_ALL);
}

function allocation(int $num_houses, int $budget, array $houses) {
  global $DEBUG;

  $total = 0;
  sort($houses);

  if ($DEBUG) {
    echo sprintf("\tnum houses = %s\n\tbudget = %s\n", count($houses), $budget);
    echo sprintf("\thouses = %s\n\n", implode(", ", $houses));
  }

  for ($i = 0; $i < count($houses) ; $i++) {
    if ($houses[$i] > $budget) {
      break;
    } else {
      $total++;
      $budget -= $houses[$i];
    }
  }

  return $total;
}

function main() {
  $test_cases = (int) fgets(STDIN);
  $answers = array();

  for ($i = 0; $i < $test_cases; $i++) {
    list($num_houses, $budget) = array_map('intval', explode(" ", fgets(STDIN)));
    $houses =  array_map('intval', explode(" ", fgets(STDIN)));

    $answers[] = allocation($num_houses, $budget, $houses);

    echo sprintf("Case #%s: %s\n", $i+1, $answers[$i]);
  }
}

main();
